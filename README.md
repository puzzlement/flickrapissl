On June 27 2014, [Flickr changed their API to be
SSL-only](http://code.flickr.net/2014/04/30/flickr-api-going-ssl-only-on-june-27th-2014/).
The [Python flickrapi library](http://stuvel.eu/flickrapi) was one of many
pieces of software that used HTTP to connect to Flickr's API, and that
therefore broke for some users on June 27.

flickrapi supports HTTPS connections as of version 1.4.4, released on June 18
2014. If you are able to upgrade to a new version of flickrapi, you can [get
the latest flickrapi from PyPI](https://pypi.python.org/pypi/flickrapi).

However, as of mid-2014, many Linux distros, including Ubuntu 14.04 (supported
until 2019), still package flickrapi version 1.2, which cannot connect to
Flickr's API and is therefore non-functional. Since developers may for various
reasons choose to use their distro's version of python-flickrapi, this project
provides a very very small Python class that overrides flickrapi's FlickrAPI
class to connect to Flickr over HTTPS rather than HTTP, and allows continued use
of the Flickr API.

If you were previously doing something like this:

    import flickrapi

    flickr_conn = flickrapi.FlickrAPI(api_key, api_secret)

You can replace it with this:

    import flickrssl

    flickr_conn = flickrssl.FlickrAPI(api_key, api_secret)
