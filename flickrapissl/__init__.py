import urllib
from flickrapi import *
from flickrapi import FlickrAPI as HTTPFlickrAPI

#In order to override a method starting with __, the new class
#name needs to be the same as the old one.

class FlickrAPI(HTTPFlickrAPI):
    def __flickr_call(self, **kwargs):
        '''Performs a Flickr API call with the given arguments. The method name
        itself should be passed as the 'method' parameter.
        
        Returns the unparsed data from Flickr::

            data = self.__flickr_call(method='flickr.photos.getInfo',
                photo_id='123', format='rest')
        '''

        post_data = self.encode_and_sign(kwargs)

        # Return value from cache if available
        if self.cache and self.cache.get(post_data):
            return self.cache.get(post_data)

        url = "https://" + FlickrAPI.flickr_host + FlickrAPI.flickr_rest_form
        flicksocket = urllib.urlopen(url, post_data)
        reply = flicksocket.read()
        flicksocket.close()

        # Store in cache, if we have one
        if self.cache is not None:
            self.cache.set(post_data, reply)

        return reply
